<?php

namespace Database\Seeders;

use App\Models\Booking;
use Illuminate\Database\Seeder;

class BookingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Booking::create([
            'customer_name' => 'Ray Pressley',
            'customer_location' => 'San Francisco',
            'customer_address' => '61 Rudden Ave San Francisco, California(CA), 94112',            
            'customer_image'    => 'ray.jpeg'
        ]);

        Booking::create([
            'customer_name' => 'Alexander Aiden',
            'customer_location' => 'San Francisco',            
            'customer_address' => '620 Jones St San Francisco, California(CA), 94102',
            'customer_image'    => 'alexander.jpg'
        ]);

        Booking::create([
            'customer_name' => 'Jacob Mason',
            'customer_location' => 'San Francisco',
            'customer_address' =>'636 Garfield St San Francisco, California(CA), 94132',            
            'customer_image'    => 'jacob.jpg'
        ]);


        Booking::create([
            'customer_name' => 'Antony',
            'customer_location' => 'San Francisco',
            'customer_address' =>'636 Garfield St San Francisco, California(CA), 94132',            
            'customer_image'    => 'jacob.jpg'
        ]);

        Booking::create([
            'customer_name' => 'George',
            'customer_location' => 'San Francisco',
            'customer_address' =>'636 Garfield St San Francisco, California(CA), 94132',            
            'customer_image'    => 'jacob.jpg'
        ]);
    }
}
